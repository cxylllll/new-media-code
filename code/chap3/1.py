# 定义函数
def f(x):
    return x**2 + 2*x + 1

# 计算梯度
def gradient(f, x, h=1e-6):
    """
    计算函数 f 在点 x 处的梯度
    f: 函数
    x: 点的位置
    h: 步长，默认为1e-6
    """
    grad = (f(x + h) - f(x - h)) / (2 * h)
    return grad

# 设置点的位置
x = 2

# 计算梯度
grad = gradient(f, x)

# 打印梯度值
print(grad)