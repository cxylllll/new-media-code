import random

# 定义骰子的面数
num_faces = 6

# 定义模拟的投掷次数
num_simulations = 10000

# 初始化一个字典来存储每个面的计数
results = {i: 0 for i in range(1, num_faces + 1)}

# 模拟投掷骰子
for _ in range(num_simulations):
    roll = random.randint(1, num_faces)  # 随机生成一个骰子点数
    results[roll] += 1

# 计算每个面的概率
probabilities = {k: v / num_simulations for k, v in results.items()}

# 打印结果
for face, probability in probabilities.items():
    print(f"点数 {face}: 概率 {probability:.4f}")
