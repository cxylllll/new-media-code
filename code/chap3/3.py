import numpy as np

# 创建一个具有形状 (2, 3, 4) 的示例张量
tensor = np.array([[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]],
                   [[13, 14, 15, 16], [17, 18, 19, 20], [21, 22, 23, 24]]])

# 在轴0上求和
sum_axis_0 = np.sum(tensor, axis=0)

# 在轴1上求和
sum_axis_1 = np.sum(tensor, axis=1)

# 在轴2上求和
sum_axis_2 = np.sum(tensor, axis=2)

# 输出结果形状
print("在轴0上求和，结果形状：", sum_axis_0.shape)
print("在轴1上求和，结果形状：", sum_axis_1.shape)
print("在轴2上求和，结果形状：", sum_axis_2.shape)
