import numpy as np
import matplotlib.pyplot as plt
# 定义函数 f(x) = x^3 - 1/x
def f(x):
    return x**3 - 1/x
# 定义切线的斜率和截距，切线方程为 y = m*x + b
x_tangent = 1
m = 3*x_tangent**2 + 1/x_tangent**2
b = f(x_tangent) - m * x_tangent
# 生成 x 值的范围
x = np.linspace(0.1, 2, 100)
# 计算函数值和切线值
y = f(x)
tangent = m * x + b
# 绘制函数图像
plt.plot(x, y, label='y = x^3 - 1/x')
# 绘制切线图像
plt.plot(x, tangent, label='Tangent at x=1', linestyle='--')
# 标记切线和函数交点
plt.plot(1, f(1), 'ro', label='Intersection (1, 0)')
# 添加标签和图例
plt.xlabel('x')
plt.ylabel('y')
plt.title('Function and Tangent Line at x=1')
plt.legend()
# 显示图像
plt.grid()
plt.show()
