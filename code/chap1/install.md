# python基础
Python和Conda是两个不同的工具，它们在软件开发和数据科学领域中有着不同的作用和用途

**python**:
 * Python是一种高级编程语言，广泛用于开发应用程序、网站、脚本等各种类型的软件。
 * Python有一个标准库，其中包含了各种模块和函数，可用于执行各种任务，从文件操作到网络编程，再到科学计算和数据分析。
 * Python的解释器可以直接安装并运行，你可以使用它来编写和执行Python代码。
 * Python的包管理器pip用于安装和管理Python软件包，这些软件包包含了各种功能和扩展，可以扩展Python的功能。

**Conda**:
  * Conda是一个跨平台的包管理工具和环境管理工具，主要用于数据科学、机器学习和科学计算领域。
  * Conda可以用来创建、管理和共享不同的Python环境，每个环境可以有不同版本的Python和各种软件包，以满足特定项目或应用程序的需求。
  * Conda还是一个包管理工具，与pip不同，它可以管理非Python软件包，例如C/C++库和依赖于系统的工具。
  * Conda还提供了一个名为Anaconda的发行版，它包含了Python解释器、常用的科学计算和数据科学库，以及Conda本身。


总结：Python是一种编程语言，而Conda是一个包管理和环境管理工具。Conda通常用于创建和管理Python环境，以及安装和管理Python和非Python软件包，特别是在数据科学和科学计算领域中非常有用。pip用于安装Python软件包，但Conda可以更广泛地管理依赖和环境。

tips: 你可以使用Conda安装Python. 推荐大家使用conda安装python并使用python

## conda安装

1. 一般两个选择，一个是官网，另一个是国内镜像网站（建议选择这个）。前者大家都懂，速度感人，所以国内一般选择后者镜像下载。
2. 我们为大家提供了conda安装包，在校内云盘，大家可以直接下载
```
    https://pan.tju.edu.cn/link/AA88245631222B4047A5E50CD23D1261C5
    文件夹名：新媒体编程基础
    有效期限：2024-12-27 11:19
    提取码：mZc7
```

```
1. Miniconda3-latest-Windows-x86_64.exe  windows安装包
2. Miniconda3-latest-MacOSX-x86_64.pkg mac Intel x86安装包
3. Miniconda3-latest-MacOSX-arm64.pkg macOS Apple M*芯片安装包
```
国内镜像网站：清华镜像网站：https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/，选择对应的版本下载即可。
![Alt text](./images/1.png)
红框中所选的为windows和macos的 （以下教程为window安装，mac系统使用brew安装即可）

3. 打开安装包，一路next，
![Alt text](./images/2.png)
4. 建议不要装在C盘，如果C盘空间充足即可忽略。
![Alt text](./images/3.png)
5. 一直按照默认选项配置，不需要勾选配置环境变量，后面统一配置
6. 安装完成以后，系统搜索配置环境变量
![Alt text](./images/4.png)
把这几条复制到里面（注意我装的是E:\Anaconda这个目录，你要根据自己实际安装目录进行改动
```
E:\Anaconda 
E:\Anaconda\Scripts 
E:\Anaconda\Library\mingw-w64\bin
E:\Anaconda\Library\usr\bin 
E:\Anaconda\Library\bin
```
这里的E:\Anaconda是你安装conda的根目录
7. 测试是否配置成功
打开cmd， 输入conda --version，如果出现版本号，说明安装成功

## conda使用
1. conda常用命令
```
conda --version # 查看conda版本
conda update conda # 更新conda
conda update anaconda # 更新anaconda
conda update python # 更新python
conda update --all # 更新所有包
conda create -n env_name python=3.8# 创建python3.环境
conda install package_name # 安装包
```

## 安装IDE 
PyCharm和Visual Studio Code（VSCode）都是流行的集成开发环境（IDE），用于Python开发，但它们有一些不同之处.

两者均可以，如果电脑配置好，推荐pycharm，如果性能一般，推荐vscode。

### vscode
1. 下载vscode
```
https://code.visualstudio.com/
```
我们提高好了安装包，在上面的天大网盘中，也可以自己在官网安装，链接
https://code.visualstudio.com/

```
VSCodeUserSetup-x64-1.84.1.exe 为windows系统
VSCode-darwin-universal.zip 为mac系统
```
![Alt text](images/5.png)

2. 点击左侧栏open folder，任意创建一个文件夹，D:/python，作为自己的工作目录。

3. 点击左侧新建文件按钮，新建一个python文件，以.py为后缀结束
![Alt text](images/7.png)

4. 安装python插件
![Alt text](images/6.png)
完成后右下角会出现你的电脑上安装的 Python 版本（下载 Python），这是 VS Code 默认使用的 Python 版本。

![Alt text](images/8.png)

然后选择conda的安装路径
![Alt text](images/9.png)
5. 运行python文件
在刚才创建的py文件中输入
```
print("hello world")
```
然后运行文件即可

### pycharm
该软件需要付费，或者申请学生优惠

https://blog.jetbrains.com/zh-hans/blog/2022/08/24/2022-jetbrains-student-program/

可按照该流程申请。


不要使用天大邮箱，被封了，使用学信网认证。

安装结束后，同理创建Python项目。

![Alt text](images/10.png)
