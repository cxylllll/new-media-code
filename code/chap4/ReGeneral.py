import re
pattern = '\wPython\W'
val = '6Python_'
print(re.search(pattern,val)) #None
pattern = '\wPython\d'
val = '7Python8'
#<re.Match object; span=(0, 8), match='7Python8'>
print(re.search(pattern,val))
pattern = 'Python\s'
val = 'Python Go'
#<re.Match object; span=(0, 7), match='Python '>
print(re.search(pattern,val))
