import urllib.request
from bs4 import BeautifulSoup
url = 'http://www.cnblogs.com/'
#发送请求
response = urllib.request.urlopen(url)
htmlContent=response.read().decode('utf-8')
soup = BeautifulSoup(htmlContent, "html.parser")
#解析编辑推荐部分的文本
editorPick = soup.find("a",  attrs={"id":"editor_pick_lnk"} )
print(editorPick)
contentText = soup.find("a", attrs={"id":"editor_pick_lnk"}).text
print(contentText)
#解析最多推荐部分的本文
contentText = soup.find("div",  attrs={'class':'card headline'} ).text
print(contentText)


