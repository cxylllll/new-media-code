import re

content = u"""
【编辑推荐】戏说领域驱动设计（十九）——外验(3/2/202)»
【最多推荐】ASP.NET Core 在 IIS 下的两种部署模式(12/24/2883)»
【最多评论】趣说 ｜ 数据库和缓存如何保证一致性？(17/22/1602)»
【新闻头条】华为自研编程语言「仓颉」火了，开启内测并被辟谣：不是中文编程(0/15/840)»
【推荐新闻】鸿蒙3.0Beta版跳票！探访Harmony OS实验室，边洗脸边追剧的镜子也智能(0/8/835)»
"""
rule = u'【编辑推荐】(.*?)»'
pickContent = re.findall(rule, content)
print(pickContent[0])
# 方括号前需要加\转义
rule = r'\【最多推荐\】(.*?)»'
mostPickContent = re.findall(rule, content)[0]
print(mostPickContent)
rule = '(.*?)\('  # 对小括号转义
txtContent = re.findall(rule, mostPickContent)[0]
print(txtContent)
# 解析评论数和点击数
rule = '\((.*?)\)'
txtContent = re.findall(rule, mostPickContent)[0]
print(txtContent.split('/'))
