
# -*- coding: utf-8 -*-
import scrapy
from cnblogsPrj.items import CnblogsItem

class BlogspiderSpider(scrapy.Spider):
    name = 'blogSpider'
    allowed_domains = ['www.cnblogs.com']
    start_urls = ['https://www.cnblogs.com/']

    def parse(self, response):
        #创建item对象
        item = CnblogsItem()
        #开始爬取
        item["title"] = response.xpath("//article[@class='post-item']//section[@class='post-item-body']//div[@class='post-item-text']//a/text()").extract()
        return item

