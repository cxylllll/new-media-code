import codecs
import json

class CnblogsPrjPipeline(object):
    #该函数需要自己创建，初始化时运行
    def __init__(self):
        self.file = codecs.open("D:/work/cnblogs.json", "wb", encoding="utf-8")

    #运行好爬虫项目后，关闭json文件
    def close_spider(self, spider):
        # 关闭json文件
        self.file.close()

    def process_item(self, item, spider):
        #通过for循环依次处理每条博文数据
        self.file.write('[')
        for index in range(0, len(item["title"])):
            # 将当前页的第j个商品的名称赋值给变量name
            title = item["title"][index]
            # 重构一条记录
            oneContent = {"title": title}
            # 写入json文件
            line = json.dumps(dict(oneContent), ensure_ascii=False)
            self.file.write(line)
            #如果不是最后一行，加分隔符逗号和换行符
            if(index !=len(item["title"]) -1 ):
                self.file.write(',\n')
        self.file.write(']')
        # 返回item
        return item
