from bs4 import BeautifulSoup
htmlContent = '<b><!-- comment --></b>'
#把解析好的对象放入soup
soup = BeautifulSoup(htmlContent, "html.parser")
comment = soup.b.string
#如下输出comment
print(comment)
#如下输出<class 'bs4.element.Comment'>
print(type(comment))
