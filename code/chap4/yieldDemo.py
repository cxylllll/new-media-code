def getNextVal(val):
    while val<3:
        val = val+1
        yield val
#<generator object getNextVal at 0x000001DCAF8AC6C8>
print(getNextVal(0))
gen = getNextVal(0)
#输出1
print(next(gen))
#输出2
print(next(gen))
#如下for循环输出3
for num in getNextVal(2):
    print(num)