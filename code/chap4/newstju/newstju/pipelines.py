# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import csv
import os

class NewstjuPipeline:


    def __init__(self) -> None:
        filename = 'D:/news.csv'
        self.file = open(filename, 'a+', encoding='utf-8-sig')
        self.writer = csv.writer(self.file)
        self.writer.writerow(
            ('title', 'views', 'time', 'url', 'abstract', 'editor', 'img_path')
        )

    def process_item(self, item, spider):
        self.writer.writerow(
            (item['title'], item['views'], item['time'], item['url'], item['abstract'], item['editor'], item['img_path']), 
        )
        return item
    
    def close_spider(self, spider):
        self.file.close()
