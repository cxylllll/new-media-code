# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class NewsItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    views = scrapy.Field()
    time = scrapy.Field()
    url = scrapy.Field()
    abstract = scrapy.Field()
    editor = scrapy.Field()
    img_path = scrapy.Field()
