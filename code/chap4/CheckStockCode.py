import re
# 匹配沪深A股和创业板股票的规则
stockPattern='^[6|3|0][0-9]{5}$'
#<re.Match object; span=(0, 6), match='300001'>
print(re.match(stockPattern,'300001'))
#<re.Match object; span=(0, 6), match='600640'>
print(re.match(stockPattern,'600640'))
#None
print(re.match(stockPattern,'900009'))


