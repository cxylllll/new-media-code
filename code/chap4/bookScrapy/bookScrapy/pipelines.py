import csv
import os

class BookscrapyPipeline(object):
    def __init__(self):
        filename = 'book.csv'
        if (os.path.exists(filename)):
            os.remove(filename)
        self.file = open(filename, 'a+', encoding='utf-8')
        self.writer = csv.writer(self.file)

    def process_item(self, item, spider):
        self.writer.writerow(
            (item['title'], item['author'], item['publishingHouse'], item['dateTime'], item['price'],
             item['score'], item['commentsNumber'])
        )
        return item

    def close_spider(self, spider):
        self.file.close()
