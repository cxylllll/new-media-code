# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BookscrapyItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()  # 标题
    author = scrapy.Field()  # 作者
    publishingHouse = scrapy.Field()  # 出版社
    dateTime = scrapy.Field()  # 出版日期
    price = scrapy.Field()  # 价格
    score = scrapy.Field()  # 打分
    commentsNumber = scrapy.Field()  # 评论数
