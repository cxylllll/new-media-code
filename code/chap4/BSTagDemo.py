from bs4 import BeautifulSoup
htmlContent = """
<html><head><title>My Title</title></head>
<body>
<p class = 'myColor'>Value</p>
</body>
</html>
"""
#把解析好的HTML内容soup
soup = BeautifulSoup(htmlContent, "html.parser")
#如下输出<title>My Title</title>
print(soup.title)
#如下输出<head><title>My Title</title></head>
print(soup.head)
#如下输出<p class = 'myColor'>Value</p>
print(soup.p)
#如下输出title，表示元素的名字
print(soup.title.name)
#如下输出p，表示元素的名字
print(soup.p.name)
#如下以键值对的形式输出{'class': ['myColor']}
print(soup.p.attrs)
#如下输出['myColor']，表示属性值
print(soup.p.attrs['class'])
