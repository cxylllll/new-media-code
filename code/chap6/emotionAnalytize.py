from snownlp import SnowNLP
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
#载入数据
commentDf = pd.read_json("./filmComment.json",lines=True,encoding='utf-8')
#新增一列，加入针对评论的情感分
index=0
while index<=len(commentDf)-1:
    commentDf.loc[index,'commentEmotion'] = SnowNLP( commentDf.loc[index,'comment'] ).sentiments
    index = index + 1
print(commentDf)
#绘制直方图
fig=plt.figure()
plt.rcParams['font.sans-serif']=['SimHei'] #显示中文
ax=fig.add_subplot(1,1,1)
ax.set_title("情感分的分布情况")
bins=np.arange(0,1.1,0.1)
ax.hist(commentDf['commentEmotion'],bins)
ax.set_xlabel('情感分')
ax.set_ylabel('数量')
xticks=np.arange(0,1.1,0.1)
ax.set_xticks(xticks)
for tick in ax.get_xticklabels():
    tick.set_rotation(45)
plt.show()
