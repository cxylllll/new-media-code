from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
#初始化对象
vectorizer = CountVectorizer()
transformer = TfidfTransformer()
docs = ["我 在 学习 编程", "编程 很 受 欢迎","我 喜欢 编程 技术"]
print(vectorizer.fit_transform(docs))
results = transformer.fit_transform(vectorizer.fit_transform(docs)).toarray().tolist()
words = vectorizer.get_feature_names_out()
print('分词结果：')
print(words)
print('tf-idf值：')
for val in results:
    print(val)