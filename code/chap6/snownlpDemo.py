from snownlp import SnowNLP
# 文本
posText = u'这书值得买'
# 分析
s = SnowNLP(posText)
print(s.words)   #输出分词结果
print(s.tf)   #输出分词结果
print(s.idf)   #输出分词结果
print(s.sentiments)   #输出情感得分
negText = u'我很生气'
s = SnowNLP(negText)
print(s.sentiments)   #输出情感得分
