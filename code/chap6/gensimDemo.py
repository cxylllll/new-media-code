from gensim import corpora
from gensim.models.tfidfmodel import TfidfModel
texts = [['我', '学习', '编程'], ['编程', '很', '受', '欢迎'], ['我', '喜欢' ,'编程'], ['我', '搭建', '编程','环境']]
dict = corpora.Dictionary(texts)
print('词典：', dict.token2id)
#生成词频，用于分析
tf = [dict.doc2bow(text) for text in texts]
print('词频：',tf)#输出词频
#构建分析模型
tfidfModel = TfidfModel(tf)
tfidf = list(tfidfModel[tf])
print('tf-idf值：',tfidf)
