# 导入了requests和json模块，用于发送HTTP请求和处理JSON数据
import requests
import json

# 需要将自己的API_KEY和SECRET_KEY填入这两个变量中。这些密钥用于身份验证，以便访问百度AI开放平台的API。
API_KEY = "填写自己的API_KEY"
SECRET_KEY = "填写自己的SECRET_KEY"

# 根据API_KEY和SECRET_KEY，通过向百度AI开放平台的令牌API发送POST请求，获取访问令牌。
url = "https://aip.baidubce.com/oauth/2.0/token"
params = {"grant_type": "client_credentials", "client_id": API_KEY, "client_secret": SECRET_KEY}
access_token = str(requests.post(url, params=params).json().get("access_token"))

# 构建情感分析请求：
url = "https://aip.baidubce.com/rpc/2.0/nlp/v1/sentiment_classify?charset=UTF-8&access_token=" + access_token
payload = json.dumps({
    "text": "配置顶级，不解释，手机需要的各个方面都很完美"   #填写你想要测试的语句
})
headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}

# 发送请求并获取响应：
response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
