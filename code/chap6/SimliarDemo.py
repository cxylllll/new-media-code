from gensim import corpora, models, similarities
import jieba
sentence1 = '我在大学里学习编程'
sentence2 = '我努力工作'
sentence3 = '我是程序员'
sentences = [sentence1, sentence2,sentence3]
comparedSen = '程序员学习编程'
texts = [jieba.lcut(text) for text in sentences]
dict = corpora.Dictionary(texts)
num_features = len(dict.token2id)
corpus = [dict.doc2bow(text) for text in texts]
tfidf = models.TfidfModel(corpus)
new_vec = dict.doc2bow(jieba.lcut(comparedSen))
# 计算相似度
index = similarities.SparseMatrixSimilarity(tfidf[corpus], num_features)
values = index[tfidf[new_vec]]
for i in range(len(values)):
    print('与第',i+1,'句话的相似度为：', values[i])
