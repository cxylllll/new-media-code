import numpy as np
boolArr = np.array([False,True],np.bool_)
print(boolArr) #[False  True]
strArr = np.array(['12','34'],np.str)
print(strArr) #['12' '34']
byteStrArr = np.array(['12','34'],np.string_)
print(byteStrArr) #[b'12' b'34']
floatArr = np.array([2.0,9.8],np.float32)
print(floatArr) #[2.   9.8]