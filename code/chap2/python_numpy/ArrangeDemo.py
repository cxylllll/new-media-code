import numpy as np
arr1 = np.arange(4)  # 只有结束项
print(arr1)  #输出结果 [0 1 2 3]，起始是0，步长是1，不包含结束项
arr2 = np.arange(1, 6)  # 起点为1，步长默认为1
print(arr2)  # 结果 [1 2 3 4 5]
arr3 = np.arange(1, 16, 3)
print(arr3)  # 结果[ 1  4  7 10 13]，不含16
arr4 = np.arange(1, 4, 0.5)
print(arr4)  # 结果[1.  1.5 2.  2.5 3.  3.5]