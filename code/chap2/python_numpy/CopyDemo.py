import numpy as np
array1 = np.array([1,2,3,4])
copiedArray = array1.copy()
array2 = copiedArray[0:2]
print(array2) #此时输出[1 2]
array1 = array1 * 3 #变更原数据
print(array1) #输出[ 3  6  9 12]，原数据已经变更
print(array2) #还是输出[1 2]，切片数据没变