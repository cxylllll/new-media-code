import numpy as np
zeroArr = np.zeros(4,dtype=np.int32)#一维，长度为4的全0数组
print(zeroArr) #[0 0 0 0]
oneArr = np.ones(4,dtype=np.float16)#一维，长度为3的全1数组
print(oneArr) #[1. 1. 1. 1.]
