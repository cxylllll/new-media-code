import numpy as np
print(np.abs(-14)) 	# 求绝对值，该表达式返回14
print(np.round_(3.7)) 	# 四舍五入，该表达式返回4.0
print(np.ceil(4.2)) 	# 求大于或等于该数的整数，该表达式返回5.0
print(np.floor(5.8)) 	# 求小于或等于该数的整数，该表达式返回5.0
print(np.cos(60*np.pi/180)) #求60度的余弦值，转换成弧度，结果是0.5
print(np.sin(30*np.pi/180)) #求30度的正弦值，转换成弧度，结果是0.5
print(np.sqrt(25)) 	# 求根号值，该表达式返回5
print(np.square(7)) 	# 求平方，该表达式返回49
print(np.sign(5)) 	# 符号函数，如果大于0则返回1，该表达式返回1
print(np.sign(-5)) 	# 符号函数，如果小0则返回-1，该表达式返回-1
print(np.sign(0)) 	# 符号函数，如果等于0则返回0，该表达式返回0
print(np.log10(1000)) 	# 求以10为底的对数，该表达式返回3.0
print(np.log2(8)) 	# 求以2为底的对数，该表达式返回3.0
print(np.exp(1)) 		# 求以e为底的幂次方，该表达式返回e
print(np.power(3,3)) 	# 求3的3次方，该表达式返回27
arr = np.array([-2,4,6])
print(np.abs(arr)) #[2 4 6]