# coding=utf-8
import numpy as np
#针对一维数组的切片操作
array1 = np.array([0,1,2,3,4,5,6])
print(array1[0:4])#[0 1 2 3]
print(array1[3:])#[3 4 5 6]
print(array1[:4])#[0 1 2 3]
print(array1[3:-1])#[3 4 5]
#针对二维数组的切片操作
array2 = np.array([[0,1,2],[3,4,5],[6,7,8]])
'''
如下语句输出前1行
[[0 1 2]]
'''
print(array2[0:1])
#如下语句输出第三列，[2 5 8]
print(array2[:,2])
'''
如下语句输出
[[3 4]
 [6 7]]
'''
print(array2[1:3,0:2])
#修改原数据，切片数据也会变
array3 = np.array([0,1,2,3,4])
array4 = array3[0:2]
print(array4) #此时输出[0 1]
array3[1] = 100 #变更原数据
print(array4) #输出[  0 100]，切片数据也改变



