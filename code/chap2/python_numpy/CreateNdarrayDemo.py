import numpy as np
ndarray1 = np.array([2,4,6])
print(ndarray1) #[2 4 6]
ndarray2 = np.array([[2,4],[6,8]]) #创建二维数组
'''
如下语句输出
[[2 4]
 [6 8]]
'''
print(ndarray2)
arrWithDType = np.array([10,  20,  30], dtype = np.int16)
print(arrWithDType) #[10 20 30]
strArr = np.array([10, 'msg',  30],dtype = np.unicode_)
print(strArr) #['10' 'msg' '30']
#errorArr = np.array([10, 'msg',  30],dtype = np.int16) #会出错