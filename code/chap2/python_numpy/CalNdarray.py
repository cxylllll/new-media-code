import numpy as np
array1 = np.array([0,1,2])
array2 = np.array([3,4,5])
print(array1+3) #[3 4 5]
print(array1*3) #[0 3 6]
print(array1+array2) #[3 5 7]
print(array1*array2) #[ 0  4 10]
print(np.square(array2)) #平方运算，输出[ 9 16 25]