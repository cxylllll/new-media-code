import numpy as np
array1 = np.array([2,4,6,8]) #一维数组
print(array1[3]) #输出第4个元素8
try:
    print(array1[6])#出现索引越界错误
except IndexError as e:
    print(e)
array2 = np.array([[1, 2, 3],[4, 5, 6],[7, 8, 9]])  # 二维数组
print(array2) #可以观察结果
print(array2[1,1]) #输出第2行第2列的元素，结果是5
print(array2[0]) #输出第1行元素，结果是[1 2 3]