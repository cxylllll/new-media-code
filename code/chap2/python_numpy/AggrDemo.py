import numpy as np
val=np.array([2,4,6,8,10])
print(np.max(val)) #找出最大值，输出为10
print(np.min(val)) #找出最小值，输出为2
print(np.median(val))#找出中位数，输出为6.0
print(np.sum(val))#输出所有元素的和，结果是30
print(np.prod(val))#输出所有元素的积，结果是3840
print(np.mean(val))#计算元素的平均数，结果是6.0
print(np.var(val))#计算元素的方差，结果是8
print(np.std(val))#计算元素的标准差，结果约为2.282

