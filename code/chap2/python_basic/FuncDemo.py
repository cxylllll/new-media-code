def calSum(maxNum):
    sum = 0
    num=0
    while num <= maxNum:
        sum = sum + num
        num = num + 1
    return sum
print(calSum(100)) # 5050
print(calSum(50))  #1275
#bad usage
print(calSum('100')) #see exception
