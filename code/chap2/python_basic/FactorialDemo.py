def factorial(num):
    if(num==1):
        return 1
    return num * factorial(num - 1)
print(factorial(3)) #6

def factorialByLoop(num):
    start = 1
    result = 1
    while start <= num:
        result = result * start
        start = start + 1
    return result
print(factorialByLoop(5)) #120


