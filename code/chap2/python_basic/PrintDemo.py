# Hello Tom, Welcome to Python!
print('hello %s, welcome to %s!' % ('Tom', 'Python'))
# Hello Tom, your age is 24, your score is 99.500000!
print('hello %s, your age is %d, your score is %f!' % ('Tom', 24, 99.5))
# Your price is 15000.50
print('Your price is %.2f' % 15000.5)
# LightSpeed is 3.000000e+05 km per second
print('LightSpeed is %e km per second' % 300000)