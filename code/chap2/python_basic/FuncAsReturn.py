def getCalFunc(maxNum):
    def calSum():
        sum = 0
        num=0
        while num <= maxNum:
            sum = sum + num
            num = num + 1
        return sum
    return calSum
func = getCalFunc(100)
print(func())
#Error Usage
print(func)
