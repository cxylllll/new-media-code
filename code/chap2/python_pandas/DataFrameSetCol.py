import pandas as pd #导入pandas
studentDict={'name':['Mary','Tim','Mike'],'age':[19,20,21],'score':[88,90,92]}
studentDf=pd.DataFrame(studentDict)
studentDf['age'] = studentDf['age']+1
studentDf['score'] = studentDf['score']*1.1
'''通过for循环输出如下的数据
Mary 20 96.80000000000001
Tim 21 99.00000000000001
Mike 22 101.2
'''
for row in studentDf.itertuples():
    print(getattr(row, 'name'), getattr(row, 'age'),getattr(row, 'score'))

