import pandas as pd #导入pandas
simpleDf=pd.DataFrame([[1,2],[3,4],[5,6]],
                       index=list('abc'),columns=list('01'))
'''如下语句输出
   0  1
a  1  2
b  3  4
c  5  6
'''
print(simpleDf)
studentDict={'name':['Mary','Tim','Mike'],'age':[19,20,21],'score':[88,90,92]}
studentDf=pd.DataFrame(studentDict)
'''如下语句输出
    name  age  salary
0  Peter   17    1000
1    Tom   25    1500
2   John   23    2000
'''
print(studentDf)
#如下输出RangeIndex(start=0, stop=3, step=1)
print(studentDf.index)
#如下输出Index(['name', 'age', 'score'], dtype='object')
print(studentDf.columns)