import pandas as pd #导入pandas
studentDict={'name':['Mary','Tim','Mike'],'age':[19,20,21],'score':[88,90,92]}
index=['a','b','c']
studentDf=pd.DataFrame(studentDict,index=index)
'''如下语句输出
    name  age  salary
a  Peter   17    1000
b    Tom   25    1500
c   John   23    2000
'''
print(studentDf)
print(studentDf.iloc[0,0]) #输出Peter
print(studentDf.loc['a','name']) #输出Peter