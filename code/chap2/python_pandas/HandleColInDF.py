import pandas as pd #导入pandas
studentDict={'name':['Mary','Tim','Mike'],'age':[19,20,21],'score':[88,90,92]}
studentDf=pd.DataFrame(studentDict)
#新创建一列
studentDf['scoreBak']=studentDf['score']
print(studentDf)  #能看到新创建的一列
studentDf['score'] = studentDf['score']+5 #更新成绩
del studentDf['scoreBak'] #删除列
print(studentDf)  #能确认scoreBak列被删除