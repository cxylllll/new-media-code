import pandas as pd #导入pandas
studentDict={'name':['Mary','Tim','Mike'],'age':[19,20,21],'score':[88,90,92]}
index=['a','b','c']
studentDf=pd.DataFrame(studentDict,index=index)
#按值排序
sortedDf = studentDf.sort_values(by=["score"] , ascending=False)
print(studentDf) #原序列数据不变
print(sortedDf) #按score降序排列
sortedDf = studentDf.sort_index(ascending=False)
print(studentDf) #原序列不变
print(sortedDf) #按索引降序排列
