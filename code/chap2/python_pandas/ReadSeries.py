import pandas as pd
import numpy as np
mySeries=pd.Series(np.arange(0, 12))
print(mySeries.head()) #输出前5行数据
print(mySeries.head(3))#输出前3行数据
print(mySeries.tail()) #输出后5行数据
print(mySeries.tail(3)) #输出后3行数据
print(mySeries.take([1,4])) #输出1号和4号索引的数据