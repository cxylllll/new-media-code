import pandas as pd #导入pandas
studentDict={'name':['Mary','Tim','Mike'],'age':[19,20,21],'score':[88,90,92]}
studentDf=pd.DataFrame(studentDict)
print(studentDf['score'].sum()) #求和
print(studentDf['score'].count()) #求个数
print(studentDf['score'].mean()) #求平均数
print(studentDf['score'].max()) #求最大值
print(studentDf['score'].min()) #求最小值
print(studentDf['score'].var()) #求方差
print(studentDf['score'].std()) #求标准差


