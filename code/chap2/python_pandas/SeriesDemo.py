import pandas as pd #导入pandas
import numpy as np
seriesVal=pd.Series([0,1,2,3])
'''如下语句输出
0    0
1    1
2    2
3    3
4    4
dtype: int32
'''
print(seriesVal)
#RangeIndex(start=0, stop=4, step=1) <class 'pandas.core.indexes.range.RangeIndex'>
print(seriesVal.index,type(seriesVal.index))
#[0 1 2 3] <class 'numpy.ndarray'>
print(seriesVal.values,type(seriesVal.values))
seriesVal[1]=10 #给索引号是1的元素赋值
#[ 0 10  2  3] <class 'numpy.ndarray'>
print(seriesVal.values,type(seriesVal.values))
anoSeries=pd.Series(np.arange(0, 5))
#print(anoSeries)
