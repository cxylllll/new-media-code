import pandas as pd #导入pandas
mySeries=pd.Series([0,1,2,3,4,5,6])
#输出[1 2 3]，不包含索引号2指定的元素
print(mySeries[1:4].values)
#[3 4 5 6]，从3号索引到末尾
print(mySeries[3:].values)
#[2 3 4 5]，从2号索引到右边数起第1个元素，但不包含结束索引的元素
print(mySeries[2:-1].values)
seriesWithIndex= pd.Series([0,1,2,3,4], index=['b','a', 'c', 'd','e'])
#输出[0 1 2 3]，包含结束索引位置
print(seriesWithIndex['b':'d'].values)
