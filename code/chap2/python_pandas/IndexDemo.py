import pandas as pd #导入pandas
seriesWithIndex= pd.Series([0,1,2,3], index=['a', 'b', 'c', 'd'])
#如下输出Index(['a', 'b', 'c', 'd'], dtype='object')
print(seriesWithIndex.index)
#如下输出[0 1 2 3]
print(seriesWithIndex.values)
print(seriesWithIndex['c']) #输出2
print(seriesWithIndex[3]) #输出3
series1= pd.Series([0,1,2,3], index=[1, 2, 3, 4])
print(series1[1]) #输出0
#print(series1[0]) #这句话会抛异常
series2= pd.Series([1,2,3,4], index=['a', 'b', 'b', 'b'])
'''
如下语句输出
b    2
b    3
b    4
dtype: int64
'''
print(series2['b'])
#通过索引删除数据
newSeries = series2.drop(['b'])
'''
如下语句输出
a    1
dtype: int64
'''
print(newSeries)