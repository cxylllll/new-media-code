import pandas as pd #导入pandas
studentDict={'name':['Mary','Tim','Mike'],'age':[19,20,21],'score':[88,90,92]}
index=['a','b','c']
studentDf=pd.DataFrame(studentDict,index=index)
#通过for循环遍历
for row in studentDf.itertuples():
    print(getattr(row, 'name'), getattr(row, 'age'),getattr(row, 'score'))