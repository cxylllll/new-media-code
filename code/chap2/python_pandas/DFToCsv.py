import pandas as pd #导入pandas
studentDict={'name':['Mary','Tim','Mike'],'age':[19,20,21],'score':[88,90,92]}
studentDf=pd.DataFrame(studentDict)
studentDf.to_csv ("student.csv" )
studentDf.to_csv("studentNoIndex.csv",index=None)
studentDf.to_csv("studentNoHead.csv", header=None)
studentDf.to_csv("studentNoHeadAndIndex.csv", index=None,header=None)
