import pandas as pd #导入pandas
series=pd.Series([0,1,2,3,4,5])
newSeries = series[0:3]
#如下输出[0 1 2]
print(newSeries[0:3].values)
series[0]=500 #改变了原Series中元素
#如下输出[500   1]
print(newSeries[0:2].values)

#对原mySeries进行了复制
copiedSeries = series.copy()
#对复制后的Series切片
newCopiedSeries = copiedSeries[0:3]
#如下输出[500   1   2]
print(newCopiedSeries.values)
series[1]=100#修改原Series
#还是输出[500   1   2]，对原值的修改不会影响到复制后的对象
print(newCopiedSeries.values)
