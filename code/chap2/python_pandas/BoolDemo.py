# coding=utf-8
import pandas as pd #导入pandas
series = pd.Series([0, 1, 2, 3, 4], index=['a', 'b', 'c', 'd', 'e'])
'''
如下输出
a    False
b    False
c    False
d    False
e     True
dtype: bool
'''
print(series > 3)
#如下输出[4]
print(series[series > 3].values)
#如下输出Index(['e'], dtype='object')
print(series[series > 3].index)