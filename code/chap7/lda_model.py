from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

# 假设有一些中文句子作为文本数据
texts = [
    "人类社会在科技的推动下，经历了从工业时代到信息时代的转变。"
    "信息技术的快速发展带来了前所未有的变革，其中人工智能作为一项重要的技术突破，引领着未来的发展方向。",
    "在自然语言处理领域，人工智能已经取得了显著的成果。"
    "语音识别技术使得计算机能够理解和转化语音信息，机器翻译系统让不同语言之间的沟通变得更加便捷。"
    "这些应用不仅提高了工作效率，还拓宽了人们获取信息的途径。",
    "然而，人工智能的发展也面临着一些挑战和争议。隐私保护、伦理道德等问题引起了社会的广泛关注。"
    "同时，人工智能的发展还需要更多的跨学科合作，以解决复杂问题和推动技术创新。",
    "总体而言，人工智能作为一项前沿技术，正在深刻地改变着我们的生活和工作方式。"
    "未来，随着科技的不断进步，人工智能有望在更多领域发挥重要作用，为人类社会带来更多创新和便利。"
]

# 将文本转换为词频矩阵
vectorizer = CountVectorizer()
X = vectorizer.fit_transform(texts)

# 训练LDA模型
lda_model = LatentDirichletAllocation(n_components=1, random_state=42)
lda_model.fit(X)

# 打印每个主题中的前5个关键词，通过对主题中的词语进行排序得到。
feature_names = vectorizer.get_feature_names_out()
for topic_idx, topic in enumerate(lda_model.components_):
    top_feature_ids = topic.argsort()[-5:][::-1]
    top_features = [feature_names[i] for i in top_feature_ids]
    print(f"Topic #{topic_idx + 1}: {', '.join(top_features)}")
