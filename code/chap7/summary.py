# LSA（潜在语义分析）
from sumy.summarizers.lsa import LsaSummarizer
# LexRank
from sumy.summarizers.lex_rank import LexRankSummarizer
# KL（KL-散度优化）
from sumy.summarizers.kl import KLSummarizer
# Reduction
from sumy.summarizers.reduction import ReductionSummarizer