from textrank4zh import TextRank4Sentence
import jieba

# 示例中文文本
with open("blog.txt", "r", encoding="utf-8") as file:
    chinese_text = file.read()

# 使用jieba分词器进行分词
word_list = jieba.lcut(chinese_text)

# 将分词后的结果拼接为字符串
chinese_text = " ".join(word_list)

# 使用 TextRank4Sentence 进行主题抽取
tr4s = TextRank4Sentence()
tr4s.analyze(chinese_text)
sentences = tr4s.get_key_sentences(num=3)
print("Top sentences:")
for sentence in sentences:
    print(sentence.sentence)
