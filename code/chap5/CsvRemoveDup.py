import pandas as pd #导入pandas
studentDf = pd.read_csv("studentWithDup.csv",encoding="utf-8")
'''如下输出
   Unnamed: 0  name  age  score
0           0  Mary   19     88
1           1   Tim   20     90
2           2  Mike   21     92
3           2  Mike   21     92
'''
print(studentDf)
studentDf.drop_duplicates(keep='first',inplace=True)
'''如下输出
   Unnamed: 0  name  age  score
0           0  Mary   19     88
1           1   Tim   20     90
2           2  Mike   21     92
'''
print(studentDf)