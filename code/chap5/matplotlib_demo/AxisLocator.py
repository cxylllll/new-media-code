import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

xmajorLocator = MultipleLocator(4) 		# 将x轴主刻度设置为4的倍数
xmajorFormatter = FormatStrFormatter('%1.1f') # 设置x轴标签的格式
xminorLocator = MultipleLocator(1) 		# 将x轴次刻度设置为0.5的倍数
ymajorLocator = MultipleLocator(0.25) 	# 将y轴主刻度设置为0.25的倍数
ymajorFormatter = FormatStrFormatter('%1.2f') # 设置y轴标签的格式
yminorLocator = MultipleLocator(0.1)	# 将y轴次刻度设置为0.1的倍数

x = np.arange(0, 20, 0.1)
#设置子图，在ax里设置坐标轴刻度
ax = plt.subplot(111)
# 设置主刻度标签的位置，标签文本的格式
ax.xaxis.set_major_locator(xmajorLocator)
ax.xaxis.set_major_formatter(xmajorFormatter)
ax.yaxis.set_major_locator(ymajorLocator)
ax.yaxis.set_major_formatter(ymajorFormatter)

# 显示次刻度标签的位置
ax.xaxis.set_minor_locator(xminorLocator)
ax.yaxis.set_minor_locator(yminorLocator)
y = np.sin(x) 	# 绘图
plt.plot(x,y)
plt.show()