import matplotlib.pyplot as plt
items = ['food','education','books','car','ohters']
sizes = [4500,3000,500,4000,500]
explode = (0,0.1,0.1,0.1,0.1)
colors=['blue','red','green','yellow','pink']
plt.pie(sizes,explode=explode,labels=items,startangle=45,colors=colors)
plt.show()
