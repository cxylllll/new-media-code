import matplotlib.pyplot as plt
import numpy as np
x = np.arange(0, 10, 0.1)
# 新建figure对象
fig=plt.figure()
# 子图1
ax1=fig.add_subplot(2,2,1)
ax1.plot(x, 3*x,label='y=3x',color='red')
ax1.legend()
# 子图2
ax2=fig.add_subplot(2,2,2)
ax2.plot(x, 5*x,label='y=5x',color='blue')
ax2.legend()
# 子图3
ax3=fig.add_subplot(2,2,4)
ax3.plot(x, 7*x,label='y=7x',color='green')
ax3.legend()
plt.show()

