import matplotlib.pyplot as plt
import numpy as np
x=np.arange(-5,6)
plt.xlim(-5,5)
plt.plot(x,x,color="green",label='y=x')
plt.plot(x,2*x,color="red",label='y=2x')
plt.plot(x,3*x,color="blue",label='y=3x')
plt.legend(loc='best') #绘制图例
plt.show()
