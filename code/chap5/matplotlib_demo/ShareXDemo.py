import matplotlib.pyplot as plt
import numpy as np
#分数明细
class1=[92,85,97,85,72,66,96,77,82,55,79,91,81]
class2=[86,57,91,93,90,74,76,89,97,58,82,92,97]
#两个子图共享x轴
figure,(axClass1, axClass2) = plt.subplots(2, sharex=True, figsize=(12,8))
#设置连续的边界值，即直方图的分布区间，比如[50,60]等
bins=np.arange(50,101,10)
#设置中文，并设置两个子图的标题
plt.rcParams['font.sans-serif']=['SimHei']
axClass1.set_title("一班的成绩分布图")
axClass2.set_title("二班的成绩分布图")
axClass1.hist(class1,bins,color='red')
axClass2.hist(class2,bins,color='blue')
plt.show()