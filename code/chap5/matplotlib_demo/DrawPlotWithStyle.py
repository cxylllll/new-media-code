from matplotlib import pyplot as plt
import matplotlib
import numpy as np
#设置x和y轴的坐标
x = np.arange(1,10,2)
plt.plot(x,x*0.5,color='#fe3413',linewidth='4',linestyle=':',alpha=0.2)
plt.plot(x,x,color='blue',linewidth='3',linestyle='--',marker='o',alpha=0.5)
plt.plot(x,x*1.5,color='pink',linewidth='5',linestyle='-.',marker='v',alpha=0.8)
plt.show()
