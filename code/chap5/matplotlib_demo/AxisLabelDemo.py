import matplotlib.pyplot as plt
import numpy as np
x=[91,84,74,81,90,78,75,93,96,73,78,91,89]
bins=np.arange(70,101,10)
#绘制直方图，统计各个区间的数值
plt.hist(x,bins,color='red')
#设置中文
plt.rcParams['font.sans-serif']=['SimHei']
plt.xlabel('成绩分组')
plt.ylabel('该区间内成绩的数量')
plt.show()