import matplotlib.pyplot as plt
items = ['吃饭开销','教育','买书','汽车','其它']
sizes = [4500,3000,500,4000,500]
explode = (0,0.1,0.1,0.1,0.1)
colors=['blue','red','green','yellow','pink']
plt.pie(sizes,explode=explode,labels=items,startangle=30,colors=colors)
plt.rcParams['font.sans-serif']=['SimHei']#设置中文
#设置标题
plt.title("本月开支",fontsize='large',fontweight='bold',verticalalignment ='center')
plt.show()
