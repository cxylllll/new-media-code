import matplotlib.pyplot as plt
x=[1,2,3,4]# x轴刻度
y=[98,92,100,90]#y轴刻度
color=['red','green','blue','pink']
x_label=['class1','class2','class3','class4']
#绘制x刻度标签
plt.xticks(x, x_label)
#绘制柱状图
plt.bar(x, y,color=color,edgecolor='yellow')
plt.show()