import matplotlib.pyplot as plt
x=[1,2,3,4]# x轴刻度
y=[93,95,91,89]#y轴刻度
color=['red','green','blue','pink']
x_label=['一班','二班','三班','四班']
#绘制x刻度标签
plt.xticks(x, x_label)
plt.rcParams['font.sans-serif']=['SimHei']#设置中文
#s设置标题
plt.title("班级平均分对照表")
#绘制柱状图
plt.bar(x, y,color=color)
plt.grid(linewidth='2',linestyle=':',color='yellow',alpha=1)
plt.show()