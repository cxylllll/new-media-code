from matplotlib import pyplot as plt
# 定义热图的横纵坐标
xLabel = ['电商事业部', '线下事业部', '保险事业部', '银行事业部','咨询事业部']
yLabel = ['1季度', '2季度', '3季度', '4季度']
#四个季度的盈利数据
data = [[38, 52, 79, 85, 84], [62, 81, 31, 67, 69], [96, 33, 19, 54, 53], [75, 62, 34, 36, 79]]
fig = plt.figure()
# 定义子图
ax = fig.add_subplot(111)
# 定义横纵坐标的刻度
ax.set_yticks(range(len(yLabel)))
ax.set_yticklabels(yLabel)
ax.set_xticks(range(len(xLabel)))
ax.set_xticklabels(xLabel)
#选择颜色的填充风格，这里选择hot
im = ax.imshow(data, cmap=plt.cm.hot_r)
# 添加颜色刻度条
plt.colorbar(im)
# 添加中文标题
plt.rcParams['font.sans-serif']=['SimHei']
plt.title("各部门盈利情况")
plt.xlabel('部门名称')
plt.ylabel('盈利（万）')
plt.show()
