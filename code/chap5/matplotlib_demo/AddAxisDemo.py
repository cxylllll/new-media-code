import matplotlib.pyplot as plt
import numpy as np
fig=plt.figure()
ax=fig.add_axes([0.1,0.1,0.9,0.9])
child_ax=fig.add_axes([0.6,0.2,0.2,0.2])
x = np.arange(0, 10.1, 0.1)
ax.plot(x, np.cos(x),color='red')
# 子图
childX = np.arange(5, 6.1, 0.1)
child_ax.plot(childX,np.cos(childX),color='green')
plt.show()

