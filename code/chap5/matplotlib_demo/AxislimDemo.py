import matplotlib.pyplot as plt
import numpy as np
x = np.arange(-5,5,1)
plt.plot(x,1.5*x)
plt.xlim(-3,3)
plt.ylim(-4,4)
plt.show()