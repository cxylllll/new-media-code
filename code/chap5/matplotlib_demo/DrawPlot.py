from matplotlib import pyplot as plt
import matplotlib
import numpy as np

# 设置x和y轴的坐标
x = np.arange(1, 10, 2)
y = np.array([4, 14, 10, 17, 20])
# 通过plot方法绘制折线
plt.plot(x, y)
plt.show()  # 通过show方法展示