import matplotlib.pyplot as plt
import numpy as np
#分数明细
x=[91,84,74,81,90,78,75,93,96,73,78,91,89]
#设置连续的边界值，即直方图的分布区间，比如[50,60]等
bins=np.arange(70,101,10)
#绘制直方图
plt.hist(x,bins,color='red')
plt.show()