import matplotlib.pyplot as plt
import numpy as np
x = np.arange(0, 8, 1)
plt.subplot(2,1,1) 	# 第一个子图在2*1的第1个位置
plt.plot(x,2*x)
plt.subplot(2,2,3) 	# 第二个子图在2*2的第3个位置
plt.plot(x,4*x)
plt.subplot(2,2,4)  	# 第三个子图在2*2的第4个位置
plt.plot(x,6*x)
plt.show()