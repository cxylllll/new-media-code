import numpy as np
import matplotlib.pyplot as plt
# 折线图
x = np.array([1,2,3,4,5])
y = np.array([22,21.5,21.8,22.2,21.9])
plt.xticks(x, ('20220301','20220302','20220303','20220304','20220307'),  rotation=45)
plt.yticks(np.arange(21,23,0.5),rotation=30)
plt.ylim(21,23)
#设置中文
plt.rcParams['font.sans-serif']=['SimHei']
plt.xlabel("交易日期")
plt.ylabel("本日收盘价")
plt.plot(x,y,color="red")
plt.show()
