import numpy as np
import matplotlib.pyplot as plt
x = np.arange(-10,10,0.1)
y = np.arange(-10,10,0.1)
#用两个坐标轴上的点在平面上画网格
gridX,gridY = np.meshgrid(x,y)
#定义绘制等值线的函数
Z = gridX*gridX+gridY*gridY
#画等值线，用渐变色来区分
contour=plt.contour(gridX,gridY,Z,cmap=plt.cm.hot)
#标记等值线
plt.clabel(contour,inline=2)
plt.show()
