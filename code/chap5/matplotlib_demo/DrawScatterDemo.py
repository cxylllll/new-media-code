import matplotlib.pyplot as plt
import numpy as np

# 新建figure对象
fig,ax=plt.subplots()
codeLines=np.array([534,441,357,287,695,496,476,689,468])
codeBugs=np.array([5,3,4,3,7,6,6,3,5])
warnings=np.array([11,9,12,5,6,7,7,5,8])
ax.scatter(codeLines,codeBugs,s=warnings*100,alpha=0.7)
plt.rcParams['font.sans-serif']=['SimHei']#设置中文
#s设置标题
plt.title("代码问题统计散点图",fontsize='large',fontweight='bold')
plt.xlabel('代码行数')
plt.ylabel('问题个数')
plt.grid()
plt.show()

