import  wordcloud
import jieba
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams['font.sans-serif'] = 'SimHei'
file = open('words.txt', 'r+', encoding='utf-8')
content = file.read()
words = jieba.cut(content, cut_all=False)
myWordCloud = wordcloud.WordCloud(
    background_color='black',
    # 设置支持中文的字体， 如果是mac的话，font_path='/Library/Fonts/Arial Unicode.ttf',
    font_path='C:\\Windows\\Fonts\\simfang.ttf',
    min_font_size=5,    # 最小字体的大小
    max_font_size=50,   # 最大字体的大小
    width=500,  # 图片宽度
).generate('/'.join(words) ) #list转成字符串，否则会报错
fig=plt.figure()
ax=fig.add_subplot(1,1,1)
ax.set_title("词云")
ax.axis('off')
ax.imshow(myWordCloud)
plt.show()
