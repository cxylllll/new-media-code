import jieba
import pandas as pd
str = '学习学习学习数据数据分析'
#先分词
result = jieba.cut(str)
wordList = list(word for word in result)
#再统计出现频率
df = pd.DataFrame(wordList,columns=['word'])
countResult = df.groupby(['word']).size().sort_values(ascending=False)
print(countResult)
'''
word
学习      3
数据      1
数据分析    1
'''