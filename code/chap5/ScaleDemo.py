# coding=utf-8
import numpy as np
origData = np.array([[12,3,60],
	                   [7,5,20],
	                   [9,6,50]])
# 计算均值
avg = origData.mean(axis=0)
# 计算标准差
std=origData.std(axis=0)
# 减去均值，除以标准差
print((origData-avg)/std)
