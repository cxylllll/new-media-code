import pandas as pd #导入pandas
studentDf = pd.read_csv("studentWithEmpty.csv",encoding="utf-8")
'''如下输出
   Unnamed: 0  name   age  score
0           0  Mary   NaN   88.0
1           1   Tim  20.0    NaN
2           2  Mike   NaN   92.0
'''
print(studentDf)
studentDf['age'].fillna(18,inplace = True) #更新age列的NaN为18
studentDf['score'].fillna(90,inplace = True) #更新score列的NaN为90
'''如下输出
   Unnamed: 0  name   age  score
0           0  Mary  18.0   88.0
1           1   Tim  20.0   90.0
2           2  Mike  18.0   92.0
'''
print(studentDf)