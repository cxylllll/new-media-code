import jieba.analyse
str = '中文文本：结巴库支持以全模式、精确模式和搜索引擎这三种模式的分词方式。'
result = jieba.analyse.extract_tags(str, topK=5, withWeight=True)
print(result)
'''
输出结果
[('模式', 1.3916565474253846), 
('以全', 0.9195975002230768),
 ('分词', 0.900265621123077), 
 ('巴库', 0.8051300344230768), 
 ('搜索引擎', 0.7481761832369231)]
'''