import nltk
import jieba
content = '我学习计算机，计算机对我帮助很大，我想找计算机方面的工作'
words = jieba.cut(content, cut_all=False)
fdist=nltk.FreqDist(words)
#展示出现频率最高的两个词组
print(fdist.tabulate(2))