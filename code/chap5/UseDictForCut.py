import jieba
str = '我在大学里学云计算和大数据'
#未使用词典
seg_list = jieba.cut(str, cut_all=False)
print("/ ".join(seg_list))
#使用词典
jieba.load_userdict("myDict.txt")
seg_list = jieba.cut(str, cut_all=False)
print("/ ".join(seg_list))
'''
我/ 在/ 大学/ 里/ 学云/ 计算/ 和/ 大/ 数据
我/ 在/ 大学/ 里学/ 云计算/ 和/ 大数据'''