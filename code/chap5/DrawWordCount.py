import nltk
import jieba
import matplotlib
matplotlib.rcParams['font.sans-serif'] = 'SimHei'
file = open('words.txt', 'r+', encoding='utf-8')
content = file.read()
words = jieba.cut(content, cut_all=False)
#统计出现次数
fdist=nltk.FreqDist(words)
fdist.plot(10)