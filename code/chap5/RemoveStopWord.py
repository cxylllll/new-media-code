import jieba
str = '我在大学里学云计算和大数据'
#使用词典
jieba.load_userdict("myDict.txt")
seg_list = jieba.cut(str, cut_all=True)
result = "/ ".join(seg_list)
print(result)
#引入停用词
stopWordFile = open('stopword.txt', 'r+', encoding='utf-8')
stopWordList = stopWordFile.read().split("\n")
newStr=''
#去除停用词
for word in result.split('/'):
    if word.strip() not in stopWordList:
        newStr += word + '/'
print(newStr)

'''
输出
我/ 在/ 大学/ 里/ 学/ 云计算/ 计算/ 和/ 大数/ 大数据/ 数据
我/ 大学/ 学/ 云计算/ 计算/ 大数/ 大数据/ 数据/
'''