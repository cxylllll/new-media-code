import jieba
str = '我在大学里学Python数据分析技能'
seg_list = jieba.cut(str, cut_all=True) #全模式
print("/ ".join(seg_list))
seg_list = jieba.cut(str, cut_all=False) #精确模式
print("/ ".join(seg_list))
seg_list = jieba.cut_for_search(str) #搜索引擎模式
print("/ ".join(seg_list))

'''
我/ 在/ 大学/ 里/ 学/ Python/ 数据/ 数据分析/ 分析/ 技能
我/ 在/ 大学/ 里学/ Python/ 数据分析/ 技能
我/ 在/ 大学/ 里学/ Python/ 数据/ 分析/ 数据分析/ 技能
'''