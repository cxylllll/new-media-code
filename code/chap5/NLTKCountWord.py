import nltk
import jieba
file = open('words.txt', 'r+', encoding='utf-8')
content = file.read()
words = jieba.cut(content, cut_all=False)
#统计出现次数
fdist=nltk.FreqDist(words)
#print(fdist.keys(),fdist.values()) #比较长，自行观察结果
#统计指定词的出现次数
print('由于',fdist['由于'])
#统计指定词的出现频率
print('由于',fdist.freq('由于'))
'''
由于 1
由于 0.0136986301369863
'''